# FingerPaintView

**Harmony demo to draw or paint using different brushes.**


Brushes available are:
>1.Neon brush
>2.Fade brush
>3.Glow brush
>4.Blur brush
>5.Emboss brush
>6.Deboss brush


## Screenshot
![输入图片说明](https://images.gitee.com/uploads/images/2021/0708/102959_996156eb_8230582.gif "fingerPaintView.gif")
![blur](https://images.gitee.com/uploads/images/2021/0409/153931_c46b8ad4_8230582.jpeg "blur.jpg")

![deboss](https://images.gitee.com/uploads/images/2021/0409/153943_fe257565_8230582.jpeg "deboss.jpg")

![emboss](https://images.gitee.com/uploads/images/2021/0409/153956_257753da_8230582.jpeg "emboss.jpg")

![fade](https://images.gitee.com/uploads/images/2021/0409/154007_8c121b72_8230582.jpeg "fade.jpg")

![glow](https://images.gitee.com/uploads/images/2021/0409/154015_8ac8872c_8230582.jpeg "glow.jpg")

![neon](https://images.gitee.com/uploads/images/2021/0409/154024_fd346ca3_8230582.jpeg "neon.jpg")

![object](https://images.gitee.com/uploads/images/2021/0409/154035_bec841b3_8230582.jpeg "object.jpg")

![s1](https://images.gitee.com/uploads/images/2021/0409/154048_d60002cf_8230582.jpeg "s1.jpg")

## Sample usage

```

drawableOnTouchView = new DrawableOnTouchView(this);
            drawableOnTouchView.setActionListener(new DrawableOnTouchView.OnActionListener() {
                @Override
                public void OnCancel() {
                    drawableOnTouchView.setClickable(false);
                }

                @Override
                public void OnDone(PixelMap bitmap) {
                    drawableOnTouchView.makeNonClickable(false);
                }

                @Override
                public void show() {
                    drawableOnTouchView.makeNonClickable(true);
                }

                @Override
                public void killSelf() {
                    //if(listener!=null)listener.killSelf(drawableOnTouchView.getBitmap());
                }
            });

            drawableOnTouchView.setColorChangedListener(new DrawableOnTouchView.OnColorChangedListener() {
                @Override
                public void onColorChanged(int color) {

                }

                @Override
                public void onStrokeWidthChanged(float strokeWidth) {

                }

                @Override
                public void onBrushChanged(int Brushid) {

                }
            });

            ComponentContainer.LayoutConfig config = new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT);
            mainFrame.addComponent(drawableOnTouchView, config);
            drawableOnTouchView.attachCanvas(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT);


```
## Changelog
## [1.0.0] - March , 2021

* 增加FingerPaintView demo；
## License
```
MIT License

Copyright (c) 2018 Anil Singh

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```
