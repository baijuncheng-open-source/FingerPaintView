package com.infi.fingerpaint;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.StackLayout;
import ohos.agp.window.dialog.ToastDialog;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.image.PixelMap;

public class MainAbility extends Ability {
    private static final String TAG = "MainAbility";
    private static HiLogLabel LOG_LABEL = new HiLogLabel(HiLog.LOG_APP, 0X00101, TAG);

    private DrawableOnTouchView drawableOnTouchView;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_main);
        StackLayout mainFrame = (StackLayout) findComponentById(ResourceTable.Id_main_frame);
        try {
            drawableOnTouchView = new DrawableOnTouchView(this);
            drawableOnTouchView.setActionListener(new DrawableOnTouchView.OnActionListener() {
                @Override
                public void OnCancel() {
                    drawableOnTouchView.setClickable(false);
                }

                @Override
                public void OnDone(PixelMap bitmap) {
                    drawableOnTouchView.makeNonClickable(false);
                }

                @Override
                public void show() {
                    drawableOnTouchView.makeNonClickable(true);
                }

                @Override
                public void killSelf() {
                    //if(listener!=null)listener.killSelf(drawableOnTouchView.getBitmap());
                }
            });

            drawableOnTouchView.setColorChangedListener(new DrawableOnTouchView.OnColorChangedListener() {
                @Override
                public void onColorChanged(int color) {

                }

                @Override
                public void onStrokeWidthChanged(float strokeWidth) {

                }

                @Override
                public void onBrushChanged(int Brushid) {

                }
            });

            ComponentContainer.LayoutConfig config = new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT);
            mainFrame.addComponent(drawableOnTouchView, config);
            drawableOnTouchView.attachCanvas(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT);
        } catch (Exception e) {
            HiLog.error(LOG_LABEL, "onStart exception : " + e.getMessage());
            new ToastDialog(this).setText(e.getMessage()).show();
        }
    }
}
