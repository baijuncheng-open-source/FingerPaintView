package com.infi.fingerpaint;

import ohos.agp.components.Attr;
import ohos.agp.components.AttrSet;
import ohos.agp.render.MaskFilter;
import ohos.app.Context;
import ohos.agp.utils.Point;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.util.Optional;

public class Utils {
    private static final String TAG = "Utils";
    private static HiLogLabel LOG_LABEL = new HiLogLabel(HiLog.LOG_APP, 0X00101, TAG);

    public static float mDensityPixels = 1;
    public static int mDensityDpi = 160;
    private static DisplayAttributes mAttribute = null;
    private static Point displaySize = new Point();

    public static Display getDefaultDisplay(Context context) {
        if (context == null) {
            HiLog.info(LOG_LABEL, "null context for getDefaultDisplay");
            return null;
        }

        DisplayManager displayManager = DisplayManager.getInstance();
        if (displayManager == null) {
            HiLog.info(LOG_LABEL, "null displayManager for getDefaultDisplay");
            return null;
        }

        Display display = displayManager.getDefaultDisplay(context).get();
        return display;
    }

    public static int convertDpToPixel(float dp) {
        float px = dp * (mDensityDpi / 160f);
        return (int)px;
    }

    public static int dp(float value) {
        if (value == 0) {
            return 0;
        }

        if (mDensityPixels != 1) {
            return (int) Math.ceil(mDensityPixels * value);
        }

        return (int)convertDpToPixel(value);
    }

    public static void checkDisplaySize(Context context/*, Configuration newConfiguration*/) {
        try {
            Display display = getDefaultDisplay(context);
            if (display != null) {
                DisplayAttributes mAttribute = display.getRealAttributes();
                display.getRealSize(displaySize);
                mDensityPixels = mAttribute.densityPixels;
                mDensityDpi = mAttribute.densityDpi;
            }
        } catch (Exception e) {
            HiLog.error(LOG_LABEL, e.getMessage());
        }
    }

    public static int getColorAttribute(AttrSet attrs, String attrName, int defaultValue) {
        int value = defaultValue;

        Optional<Attr> optional = attrs.getAttr(attrName);
        if (optional.isPresent()) {
            value = optional.get().getColorValue().getValue();
        }

        return value;
    }

    public static boolean getBooleanAttribute(AttrSet attrs, String attrName, boolean defaultValue) {
        boolean value = defaultValue;

        Optional<Attr> optional = attrs.getAttr(attrName);
        if (optional.isPresent()) {
            value = optional.get().getBoolValue();
        }

        return value;
    }

    public static int getIntAttribute(AttrSet attrs, String attrName, int defaultValue) {
        int value = defaultValue;

        Optional<Attr> optional = attrs.getAttr(attrName);
        if (optional.isPresent()) {
            value = optional.get().getIntegerValue();
        }

        return value;
    }

    public static float getFloatAttribute(AttrSet attrs, String attrName, float defaultValue) {
        float value = defaultValue;

        Optional<Attr> optional = attrs.getAttr(attrName);
        if (optional.isPresent()) {
            value = optional.get().getFloatValue();
        }

        return value;
    }

    public static String getStringAttribute(AttrSet attrs, String attrName, String defaultValue) {
        String value = defaultValue;

        Optional<Attr> optional = attrs.getAttr(attrName);
        HiLog.info(LOG_LABEL, "getStringAttribute, attrName = " + attrName + " optional.isPresent() = " + optional.isPresent());
        if (optional.isPresent()) {
            value = optional.get().getStringValue();
            HiLog.info(LOG_LABEL, "getStringAttribute, attrName = " + attrName + " value = " + value);
        }

        return value;
    }

    /**
     * Return the red component of a color int. This is the same as saying
     * (color >> 16) & 0xFF
     */
    public static int red(int color) {
        return (color >> 16) & 0xFF;
    }

    /**
     * Return the green component of a color int. This is the same as saying
     * (color >> 8) & 0xFF
     */
    public static int green(int color) {
        return (color >> 8) & 0xFF;
    }

    /**
     * Return the blue component of a color int. This is the same as saying
     * color & 0xFF
     */
    public static int blue(int color) {
        return color & 0xFF;
    }

    public static MaskFilter idToMaskFilter(int id, float radius) {
        switch (id) {
            case BrushType.BRUSH_NEON:
                return Brush.setNeonBrush(radius);
            case BrushType.BRUSH_BLUR:
                return Brush.setBlurBrush(radius);
            case BrushType.BRUSH_INNER:
                return Brush.setInnerBrush(radius);
            case BrushType.BRUSH_EMBOSS:
                return Brush.setEmbossBrush();
            case BrushType.BRUSH_DEBOSS:
                return Brush.setDebossBrush();
            default:
                return Brush.setSolidBrush(radius);
        }
    }

    public static int stringToInt(String id) {
        HiLog.info(LOG_LABEL, "stringToInt, id = " + id);
        switch (id) {
            case "normal":
                return BrushType.BRUSH_SOLID;
            case "neon":
                return BrushType.BRUSH_NEON;
            case "inner":
                return BrushType.BRUSH_INNER;
            case "blur":
                return BrushType.BRUSH_BLUR;
            case "emboss":
                return BrushType.BRUSH_EMBOSS;
            case "deboss":
                return BrushType.BRUSH_DEBOSS;
            default:
                return BrushType.BRUSH_SOLID;
        }
    }
}
