package com.infi.fingerpaint;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.render.PixelMapHolder;
import ohos.agp.render.Path;
import ohos.agp.render.Paint;
import ohos.agp.render.MaskFilter;
import ohos.agp.render.Canvas;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.agp.utils.Color;
import ohos.media.image.PixelMap;
import ohos.media.image.PixelMap.InitializationOptions;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Size;
import ohos.multimodalinput.event.TouchEvent;

import java.util.ArrayList;

/**
 * Created by INFIi on 1/21/2017.
 */
public class FingerPaintView extends Image implements Component.TouchEventListener, Component.DrawTask {
    private static final String TAG = "FingerPaintView";
    private static HiLogLabel LOG_LABEL = new HiLogLabel(HiLog.LOG_APP, 0X00101, TAG);

    private PixelMap mBitmap;
    private Canvas mCanvas;
    private Path mPath;
    private Paint mBitmapPaint;
    private Paint mPaint;
    private ArrayList<PaintData> pathList = new ArrayList<>();
    private ArrayList<PaintData> undoList = new ArrayList<>();
    private int width;
    private int height;
    private float radius = 28;
    private Paint undoPaint;
    private boolean redraw;
    private int lastcolor;
    private float lastStrokeWidth;
    private MaskFilter lastMaskFilter;
    private int watcher = 0;
    private Preferences mPreferences;
    private float mX;
    private float mY;
    private static final float TOUCH_TOLERANCE = 4;

    private OnColorPickerChanged colorPickerChanged = null;
    private OnUndoEmptyListener undoEmptyListener = null;
    private boolean mfromColorChange = false;
    private DrawableOnTouchView drawableOnTouchView;
    private boolean down = false;

    public interface OnUndoEmptyListener {
        void undoListEmpty();
        void redoListEmpty();
        void refillUndo();
        void OnUndoStarted();
        void OnUndoCompleted();
        void onTouchDown();
        void onTouchUp();
    }

    public interface OnColorPickerChanged {
        void onColorChanged(int color);
        void onStrokeWidthChanged(float strokeWidth);
        void onBrushChanged(int Brushid);
    }

    public void setColorPickerChanged(OnColorPickerChanged colorPickerChanged) {
        this.colorPickerChanged = colorPickerChanged;
    }

    public void setUndoEmptyListener(OnUndoEmptyListener undoEmptyListener) {
        this.undoEmptyListener = undoEmptyListener;
    }

    public FingerPaintView(Context c) {
        super(c);
        init(c);
    }

    public FingerPaintView(Context context, AttrSet attrs) {
        super(context, attrs);
        init(context);
    }

    public FingerPaintView(Context context, AttrSet attrs, String defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    private void init(Context c) {
        setTouchEventListener(this);
        addDrawTask(this);

        DatabaseHelper databaseHelper = new DatabaseHelper(c);
        mPreferences = databaseHelper.getPreferences("paint");

        mPaint = new Paint();
        lastcolor = 0xffff0000;
        lastStrokeWidth = mPreferences.getFloat("stroke",12.0f);
        lastMaskFilter = Utils.idToMaskFilter(mPreferences.getInt("id", BrushType.BRUSH_SOLID), radius = 28);
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setColor(new Color(lastcolor));
        mPaint.setStyle(Paint.Style.STROKE_STYLE);
        mPaint.setStrokeJoin(Paint.Join.ROUND_JOIN);
        mPaint.setStrokeCap(Paint.StrokeCap.ROUND_CAP);
        mPaint.setStrokeWidth(lastStrokeWidth);

        mPaint.setMaskFilter(lastMaskFilter);
        undoPaint = mPaint;
        redraw = false;
        DisplayManager displayManager = DisplayManager.getInstance();
        Display display = displayManager.getDefaultDisplay(c).get();
        width = display.getRealAttributes().width;
        height = display.getRealAttributes().height;
        InitializationOptions options = new InitializationOptions();
        options.pixelFormat = PixelFormat.ARGB_8888;
        options.size = new Size(width, height);
        mBitmap = PixelMap.create(options);
        mCanvas = new Canvas();
        mPath = new Path();
        mBitmapPaint = new Paint();
        mBitmapPaint.setDither(true);
    }

    public boolean hasDrawn() {
        return pathList.size() > 0;
    }

    public void clearWatcerList() {
        watcher = 0;
    }

    public void clearFromLastWatcher() {
        for (int i = 0; i < watcher; i++) {
            pathList.remove(pathList.size() - 1);
        }
        watcher = 0;
        invalidate();
    }

    public PixelMap getmBitmap() {
        return mBitmap;
    }

    public void setBrushColor(int color,boolean fromColorChange) {
        mfromColorChange = fromColorChange;
        if (colorPickerChanged != null) {
            colorPickerChanged.onColorChanged(color);
        }
        mPaint.setColor(new Color(color));
        lastcolor = color;
        redraw = true;
    }

    public void setBrushStrokeWidth(float width) {

        if (colorPickerChanged != null) {
            colorPickerChanged.onStrokeWidthChanged(width);
        }
        if (width > 0.f) {
            lastStrokeWidth = width;
        }
        if (lastStrokeWidth == 0.f) {
            lastStrokeWidth = 12.f;
        }
        mPaint.setStrokeWidth(lastStrokeWidth);
        undoPaint = mPaint;
        mPreferences.putFloat("stroke", lastStrokeWidth).flushSync();
    }

    public void setBrushType(int id) {
        if (colorPickerChanged != null) {
            colorPickerChanged.onBrushChanged(id);
        }
        lastMaskFilter = Utils.idToMaskFilter(id, radius);
        mPaint.setMaskFilter(lastMaskFilter);
        undoPaint = mPaint;
        mPreferences.putInt("id", id).flushSync();
    }

    @Override
    public  void onDraw(Component component, Canvas canvas) {
        try {
            canvas.drawColor(Color.TRANSPARENT.getValue(), Canvas.PorterDuffMode.SRC);
            PixelMapHolder holder = new PixelMapHolder(mBitmap);
            canvas.drawPixelMapHolder(holder, 0, 0, mBitmapPaint);
            if (!pathList.isEmpty()) {
                redraw(canvas);
            }
            if (!redraw) {
                canvas.drawPath(mPath, mPaint);
            } else {
                mPath.reset();
                undoPaint.setMaskFilter(lastMaskFilter);
                mPath.moveTo(mX, mY);
                redraw = false;
            }
        } catch (Exception e) {
            HiLog.error(LOG_LABEL, "onDraw, exception = " + e.getMessage());
        }
    }

    private void touch_start(float x, float y) {
        if (undoEmptyListener != null) {
            undoEmptyListener.onTouchDown();
        }
        mPath.moveTo(x, y);
        mX = x;
        mY = y;
    }

    private void touch_move(float x, float y) {
        float dx = Math.abs(x - mX);
        float dy = Math.abs(y - mY);
        if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
            mPath.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
            mX = x;
            mY = y;
        }
    }

    private void touch_up(boolean isPoint) {
        if (undoEmptyListener != null && pathList.size() == 0) {
            undoEmptyListener.refillUndo();
        }
        if (undoEmptyListener != null) {
            undoEmptyListener.onTouchUp();
        }
        mPath.lineTo(mX, mY);
        // commit the path to our offscreen
        mCanvas.drawPath(mPath, mPaint);
        // kill this so we don't double draw

        if (isPoint) {
            PaintData pd = new PaintData(lastcolor, lastStrokeWidth, lastMaskFilter, mPath);
            pathList.add(pd);
            watcher++;
        }
        mPath.reset();
    }

    public void onUndo() {
        try {
            if (undoEmptyListener != null) {
                undoEmptyListener.OnUndoStarted();
            }

            if (pathList.size() > 0) {
                redraw = true;
                undoList.add(pathList.remove(pathList.size() - 1));
                if (pathList.isEmpty() && undoEmptyListener != null) {
                    undoEmptyListener.undoListEmpty();
                }
                touch_up(false);
                invalidate();
            } else {
                if (undoEmptyListener != null) {
                    undoEmptyListener.undoListEmpty();
                }
            }
        } catch (Exception e) {
            HiLog.error(LOG_LABEL, e.getMessage());
        }
        if (undoEmptyListener != null) {
            undoEmptyListener.OnUndoCompleted();
        }
    }

    private void redraw(Canvas canvas) {
        for (PaintData pd : pathList) {
            undoPaint = setPaintAttrs();
            undoPaint.setColor(new Color(pd.color));
            undoPaint.setStrokeWidth(pd.strokeWidth);
            if (pd.maskFilter != null) {
                undoPaint.setMaskFilter(pd.maskFilter);
            }
            canvas.drawPath(pd.path, undoPaint);
        }
        redraw = false;
    }


    private Paint setPaintAttrs() {
        Paint mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setStyle(Paint.Style.STROKE_STYLE);
        mPaint.setStrokeJoin(Paint.Join.ROUND_JOIN);
        mPaint.setStrokeCap(Paint.StrokeCap.ROUND_CAP);
        return mPaint;
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent event) {
        int index = event.getIndex();
        float x = event.getPointerScreenPosition(index).getX();
        float y = event.getPointerScreenPosition(index).getY() - 300f;

        if (x <= 900 && event.getPointerScreenPosition(index).getY() >= 400) {
            switch (event.getAction()) {
                case TouchEvent.PRIMARY_POINT_DOWN:
                    down = true;
                    touch_start(x, y);
                    mfromColorChange = false;
//                    invalidate();
                    break;
                case TouchEvent.POINT_MOVE:
                    if (!down) {
                        down = true;
                        touch_start(x, y);
                        mfromColorChange = false;
                    }
                    touch_move(x, y);
                    invalidate();
                    break;
                case TouchEvent.PRIMARY_POINT_UP:
                    down = false;
                    if (!mfromColorChange) {
                        touch_up(true);
//                        invalidate();
                    }
                    break;
            }
        }
        return true;
    }

    private class PaintData {
        public int color;
        public float strokeWidth;
        public MaskFilter maskFilter = Brush.setSolidBrush(20);
        public Path path = new Path();
        public PaintData(int clr,float strokeWidth,MaskFilter maskFilter,Path path) {
            this.color = clr;
            this.maskFilter = maskFilter;
            this.strokeWidth = strokeWidth;
            this.path.set(path);
        }
    }
}


