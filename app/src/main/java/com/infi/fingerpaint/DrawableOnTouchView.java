package com.infi.fingerpaint;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Slider;
import ohos.agp.components.StackLayout;
import ohos.agp.components.Text;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.components.element.VectorElement;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.utils.Rect;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.image.PixelMap;
import ohos.multimodalinput.event.TouchEvent;

/**
 * Created by INFIi on 1/21/2017.
 */
/* interfaces onActionListener, onColorChangedListener*/
public class DrawableOnTouchView extends StackLayout {
    private static final String TAG = "DrawableOnTouchView";
    private static HiLogLabel LOG_LABEL = new HiLogLabel(HiLog.LOG_APP, 0X00101, TAG);

    private Image undo;
    private Image painterIcon;
    private Image done;
    private Image cancel;

    private ShaderTextView normal_brush;
    private ShaderTextView neon_brush;
    private ShaderTextView inner_brush;
    private ShaderTextView blur_brush;
    private ShaderTextView emboss_brush;
    private ShaderTextView deboss_brush;

    private ColorPicker colorPicker;
    private FingerPaintView fingerPaintView;
    private DirectionalLayout select_brush_frame;
    private DirectionalLayout strokeWidth_frame;
    private DirectionalLayout draw_action_layout;
    private StackLayout undo_frame;
    private Slider strokeSeekbar;
    private Context context;
    private Text strokeWidthstatus;
    private StackLayout main_frame;
    private Image onDoneIv;
    private boolean controlsHidden = false;
    private StackLayout canvasFrame;

    private OnActionListener actionListener;
    private OnColorChangedListener colorChangedListener;

    public interface OnActionListener {
        void OnCancel();
        void OnDone(PixelMap bitmap);
        void show();
        void killSelf();
    }

    public interface  OnColorChangedListener {
        void onColorChanged(int color);
        void onStrokeWidthChanged(float strokeWidth);
        void onBrushChanged(int Brushid);
    }

    public void setActionListener(OnActionListener actionListener) {
        this.actionListener = actionListener;
    }

    public void setColorChangedListener(final OnColorChangedListener colorChangedListener) {
        this.colorChangedListener = colorChangedListener;
        fingerPaintView.setColorPickerChanged(new FingerPaintView.OnColorPickerChanged() {
            @Override
            public void onColorChanged(int color) {
                DrawableOnTouchView.this.colorChangedListener.onColorChanged(color);
            }

            @Override
            public void onStrokeWidthChanged(float strokeWidth) {
                DrawableOnTouchView.this.colorChangedListener.onStrokeWidthChanged(strokeWidth);
            }

            @Override
            public void onBrushChanged(int Brushid) {
                DrawableOnTouchView.this.colorChangedListener.onBrushChanged(Brushid);
            }
        });
    }

    public DrawableOnTouchView(Context context) {
        super(context);
        init(context);
    }

    public DrawableOnTouchView(Context context, AttrSet attrs) {
        super(context, attrs);
        init(context);
    }

    public DrawableOnTouchView(Context context, AttrSet attrs, String defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    private void init(Context context) {
        this.context = context;
        try {
            Component layout = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_drawable_view_layout,null, false);
            addComponent(layout);
            bindViews(layout);
            setClickables();
        } catch (Exception e) {
            HiLog.error(LOG_LABEL, "onInit() exception : ", e.getMessage());
            new ToastDialog(getContext()).setText(e.getMessage()).show();
        }
    }

    public  void attachCanvas() {
        LayoutConfig params = new LayoutConfig(LayoutConfig.MATCH_PARENT, LayoutConfig.MATCH_PARENT);
        canvasFrame.addComponent(fingerPaintView, params);
    }

    public void attachCanvas(int width,int height) {
        LayoutConfig params = new LayoutConfig(width, height);
        fingerPaintView.setClipAlignment(LayoutAlignment.CENTER);
        canvasFrame.addComponent(fingerPaintView, params);
    }

    public void hideActions() {
        draw_action_layout.setVisibility(HIDE);
    }

    public void flipOnTouch(boolean isVisible) {
        int v = isVisible ? HIDE : VISIBLE;
        colorPicker.setVisibility(v);
        colorPicker.init(getContext());
        select_brush_frame.setVisibility(HIDE);
        strokeWidth_frame.setVisibility(HIDE);
        undo_frame.setVisibility(v);
        controlsHidden = isVisible ? false : true;
    }

    public void clearFromLastWatcher() {
        fingerPaintView.clearFromLastWatcher();
        fingerPaintView.clearWatcerList();
    }

    public boolean hasDrawn() {
        return fingerPaintView.hasDrawn();
    }

    public void clearWatcherList() {
        fingerPaintView.clearWatcerList();
    }

    private void enableShader(ShaderTextView shaderTextView, int filterId) {
        shaderTextView.setFilterId(filterId);
        shaderTextView.setRadius(16);
        shaderTextView.enableMask();
    }

    private void bindViews(Component layout) {
        undo = (Image)layout.findComponentById(ResourceTable.Id_undo_btn);
        Rect componentPosition = undo.getComponentPosition();
        HiLog.error(LOG_LABEL,"componentPosition:right=" + componentPosition.right + "componentPosition.bottom=" + componentPosition.bottom);
        undo.setBackground(new VectorElement(getContext(), ResourceTable.Graphic_ic_undo));
        painterIcon = (Image)layout.findComponentById(ResourceTable.Id_show_stroke_bar);
        painterIcon.setBackground(new VectorElement(getContext(), ResourceTable.Graphic_ic_gestures));
        painterIcon.setClickable(false);
        painterIcon.setVisibility(HIDE);

        // init brushes
        normal_brush = (ShaderTextView)layout.findComponentById(ResourceTable.Id_normal_brush);
        enableShader(normal_brush, BrushType.BRUSH_SOLID);

        neon_brush = (ShaderTextView)layout.findComponentById(ResourceTable.Id_neon_brush);
        enableShader(neon_brush, BrushType.BRUSH_NEON);

        inner_brush = (ShaderTextView)layout.findComponentById(ResourceTable.Id_inner_brush);
        enableShader(inner_brush, BrushType.BRUSH_INNER);

        blur_brush = (ShaderTextView)layout.findComponentById(ResourceTable.Id_blur_brush);
        enableShader(blur_brush, BrushType.BRUSH_BLUR);

        emboss_brush = (ShaderTextView)layout.findComponentById(ResourceTable.Id_emboss_brush);
        enableShader(emboss_brush, BrushType.BRUSH_EMBOSS);

        deboss_brush = (ShaderTextView)layout.findComponentById(ResourceTable.Id_deboss_brush);
        enableShader(deboss_brush, BrushType.BRUSH_DEBOSS);

        colorPicker = (ColorPicker) layout.findComponentById(ResourceTable.Id_color_picker);
        canvasFrame = (StackLayout) layout.findComponentById(ResourceTable.Id_canvas_frame);

        fingerPaintView = new FingerPaintView(context);
        DatabaseHelper databaseHelper = new DatabaseHelper(getContext());
        Preferences preferences = databaseHelper.getPreferences("paint");
        float location = preferences.getFloat("last_color_location", 0.5f);
        fingerPaintView.setBrushColor(colorPicker.colorForLocation(location),false);
        fingerPaintView.setBrushStrokeWidth(12.f);

        select_brush_frame = (DirectionalLayout) layout.findComponentById(ResourceTable.Id_brush_option_frame);
        select_brush_frame.setTouchEventListener(new TouchEventListener() {
            @Override
            public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
                return true;
            }
        });
        strokeWidth_frame = (DirectionalLayout) layout.findComponentById(ResourceTable.Id_stroke_width_layout);

        strokeSeekbar = (Slider) layout.findComponentById(ResourceTable.Id_stroke_width_seekbar);
        strokeSeekbar.setMaxValue(50);
        strokeSeekbar.setProgressValue(14);
        strokeSeekbar.setProgressColor(new Color(Color.getIntColor("#e04800")));

        strokeWidthstatus = (Text)layout.findComponentById(ResourceTable.Id_stroke_width_status);

        cancel = (Image)layout.findComponentById(ResourceTable.Id_draw_canceled);
        done = (Image)layout.findComponentById(ResourceTable.Id_draw_done);

        main_frame = (StackLayout) layout.findComponentById(ResourceTable.Id_draw_main_frame);
        draw_action_layout = (DirectionalLayout) layout.findComponentById(ResourceTable.Id_draw_action_layout);

        onDoneIv = (Image)layout.findComponentById(ResourceTable.Id_onDone_iv);

        undo_frame = (StackLayout) layout.findComponentById(ResourceTable.Id_undo_frame);
        hideChecks();
    }

    public void hideChecks() {
        cancel.setVisibility(HIDE);
        done.setVisibility(HIDE);
    }

    public void showChecks() {
        cancel.setVisibility(VISIBLE);
        done.setVisibility(VISIBLE);
    }

    private void setClickables() {
        fingerPaintView.setUndoEmptyListener(new FingerPaintView.OnUndoEmptyListener() {
            @Override
            public void undoListEmpty() {
                undo.setAlpha(0.4f);
            }

            @Override
            public void redoListEmpty() {

            }

            @Override
            public void refillUndo() {
                undo.setAlpha(1.0f);
            }

            @Override
            public void OnUndoStarted() {
            }

            @Override
            public void OnUndoCompleted() {

            }

            @Override
            public void onTouchDown() {
                flipOnTouch(true);
            }

            @Override
            public void onTouchUp() {
                if (!controlsHidden)
                    flipOnTouch(false);
            }
        });

        undo.setClickedListener(new ClickedListener() {
            @Override
            public void onClick(Component v) {
                HiLog.error(LOG_LABEL,"undo.setClickedListener:");
                fingerPaintView.onUndo();
            }
        });

        painterIcon.setClickedListener(new ClickedListener() {
            @Override
            public void onClick(Component v) {
                strokeWidth_frame.setVisibility(strokeWidth_frame.getVisibility() == Component.VISIBLE ? Component.INVISIBLE : Component.VISIBLE);
            }
        });

        strokeSeekbar.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int i1, boolean var3) {
                strokeWidthstatus.setText("Stroke Width:" + strokeSeekbar.getProgress());
            }
            @Override
            public void onTouchStart(Slider slider) {
            }
            @Override
            public void onTouchEnd(Slider slider) {
                strokeSeekbar.setProgressValue(strokeSeekbar.getProgress());
                fingerPaintView.setBrushStrokeWidth(strokeSeekbar.getProgress());
            }
        });
        strokeSeekbar.setTouchEventListener(new TouchEventListener() {
            @Override
            public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
                return true;
            }
        });
        colorPicker.setColorPickerListener(new ColorPicker.ColorPickerListener() {
            @Override
            public void onBeganColorPicking() {
            }

            @Override
            public void onColorValueChanged(int color) {

            }

            @Override
            public void onFinishedColorPicking(int color) {
                fingerPaintView.setBrushColor(color,true);
            }

            @Override
            public void onSettingsPressed() {
                showBrushOptions();
            }
        });

        normal_brush.setClickedListener(new ClickedListener() {
            @Override
            public void onClick(Component v) {
                fingerPaintView.setBrushType(BrushType.BRUSH_SOLID);
                showBrushOptions();
            }
        });

        neon_brush.setClickedListener(new ClickedListener() {
            @Override
            public void onClick(Component v) {
                fingerPaintView.setBrushType(BrushType.BRUSH_NEON);
                showBrushOptions();
            }
        });

        inner_brush.setClickedListener(new ClickedListener() {
            @Override
            public void onClick(Component v) {
                fingerPaintView.setBrushType(BrushType.BRUSH_INNER);
                showBrushOptions();
            }
        });

        blur_brush.setClickedListener(new ClickedListener() {
            @Override
            public void onClick(Component v) {
                fingerPaintView.setBrushType(BrushType.BRUSH_BLUR);
                showBrushOptions();
            }
        });

        emboss_brush.setClickedListener(new ClickedListener() {
            @Override
            public void onClick(Component v) {
                fingerPaintView.setBrushType(BrushType.BRUSH_EMBOSS);
                showBrushOptions();
            }
        });

        deboss_brush.setClickedListener(new ClickedListener() {
            @Override
            public void onClick(Component v) {
                fingerPaintView.setBrushType(BrushType.BRUSH_DEBOSS);
                showBrushOptions();
            }
        });

        cancel.setClickedListener(new ClickedListener() {
            @Override
            public void onClick(Component v) {
                if (actionListener != null)actionListener.OnCancel();
            }
        });

        done.setClickedListener(new ClickedListener() {
            @Override
            public void onClick(Component v) {
                onDoneIv.setPixelMap(fingerPaintView.getmBitmap());
                fingerPaintView.setVisibility(HIDE);
                if (actionListener != null) {
                    actionListener.OnDone(fingerPaintView.getmBitmap());
                }
                hideOnDone();
            }
        });
    }

    private void OnDoneAnimation() {
        int centerX = (int) (done.getContentPositionX() + (done.getWidth() / 2));
        int centerY = (int) (done.getContentPositionY() + (done.getHeight() / 2));
        done.setVisibility(HIDE);
        cancel.setVisibility(HIDE);
        AnimatorProperty reveal = draw_action_layout.createAnimatorProperty();
        reveal.moveFromX(centerX).moveToX(getWidth()).moveFromY(centerY).moveToY(getHeight());
        reveal.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {

            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                draw_action_layout.setVisibility(HIDE);
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });
        reveal.start();

    }

    public PixelMap getBitmap() {
        return fingerPaintView.getmBitmap();
    }

    public void hideOnDone() {
        //OnDoneAnimation();
        controlsHidden = true;
        onDoneIv.setPixelMap(fingerPaintView.getmBitmap());
        onDoneIv.setVisibility(VISIBLE);
        colorPicker.setVisibility(HIDE);
        select_brush_frame.setVisibility(HIDE);
        undo.setVisibility(HIDE);
        strokeWidth_frame.setVisibility(HIDE);
        Element element = new ShapeElement(getContext(), ResourceTable.Color_transparent);
        main_frame.setBackground(element);
        draw_action_layout.setVisibility(HIDE);
        fingerPaintView.setVisibility(HIDE);
        painterIcon.setVisibility(HIDE);
    }

    public void makeNonClickable(boolean show) {
        painterIcon.setClickable(true);
        undo_frame.setClickable(true);
        onDoneIv.setClickable(false);
        colorPicker.setClickable(show);
        select_brush_frame.setClickable(false);
        undo.setClickable(true);
        strokeWidth_frame.setClickable(false);
        draw_action_layout.setClickable(false);
        fingerPaintView.setClickable(show);
    }

    public void show() {
       // OnDoneAnimation();
        controlsHidden = false;
        onDoneIv.setPixelMap(null);
        onDoneIv.setVisibility(HIDE);
        fingerPaintView.setVisibility(VISIBLE);
        colorPicker.setVisibility(VISIBLE);
        select_brush_frame.setVisibility(HIDE);
        undo.setVisibility(VISIBLE);
        strokeWidth_frame.setVisibility(HIDE);
        painterIcon.setVisibility(VISIBLE);
        Element element = new ShapeElement(getContext(), ResourceTable.Color_transparent);
        main_frame.setBackground(element);
    }

    private void showBrushOptions() {
        boolean show = select_brush_frame.getVisibility() != VISIBLE;
        if (show) {
            select_brush_frame.setVisibility(VISIBLE);
        } else {
            select_brush_frame.setVisibility(INVISIBLE);
        }
        strokeWidth_frame.setVisibility(show ? VISIBLE : HIDE);
    }
}
