package com.infi.fingerpaint;

/**
 * Created by INFIi on 1/21/2017.
 */
import ohos.agp.render.MaskFilter;

public class Brush {
    public static MaskFilter setNeonBrush(float radius) {
        return  new MaskFilter(radius, MaskFilter.Blur.OUTER);
    }

    public static MaskFilter setSolidBrush(float radius) {
        return  new MaskFilter(radius, MaskFilter.Blur.SOLID);
    }

    public static MaskFilter setInnerBrush(float radius) {
        return  new MaskFilter(radius, MaskFilter.Blur.INNER);
    }

    public static MaskFilter setBlurBrush(float radius) {
        return  new MaskFilter(radius, MaskFilter.Blur.NORMAL);
    }

    public static MaskFilter setEmbossBrush() {
        return new MaskFilter(new float[]{0f, 1f, 0.5f},0.8f,3f,0f);
    }

    public static MaskFilter setDebossBrush() {
        return new MaskFilter(new float[]{0f, -1f, 0.5f},0.8f,15f,0f);
    }
}