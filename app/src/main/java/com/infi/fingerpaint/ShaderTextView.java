package com.infi.fingerpaint;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Text;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * Created by INFIi on 1/22/2017.
 */

public class ShaderTextView extends Text {
    private static final String TAG = "ShaderTextView";
    private static HiLogLabel LOG_LABEL = new HiLogLabel(HiLog.LOG_APP, 0X00101, TAG);

    private Paint mTextPaint;
    private int mFilterId;
    private int mRadius;

    public ShaderTextView(Context context) {
        super(context);

    }
    public ShaderTextView(Context context, AttrSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public ShaderTextView(Context context, AttrSet attrs, String defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }

    public void setFilterId(int filterId) {
        this.mFilterId = filterId;
    }

    public void setRadius(int radius) {
        this.mRadius = radius;
    }
    //to be called after setting id and radius
    public void enableMask() {
        mTextPaint.setMaskFilter(Utils.idToMaskFilter(mFilterId, mRadius));
    }

    private void init(Context context, AttrSet attrs) {
        mTextPaint = new Paint();
        mTextPaint.setAntiAlias(true);
        mTextPaint.setColor(new Color(0xff00cc));

        mFilterId = Utils.stringToInt(Utils.getStringAttribute(attrs, "mask_id", "normal"));
        mRadius = Utils.getIntAttribute(attrs, "mask_radius", 14);


        mTextPaint.setMaskFilter(Utils.idToMaskFilter(mFilterId, mRadius));
    }
}
