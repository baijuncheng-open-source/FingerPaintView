package com.infi.fingerpaint;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.colors.ColorConverter;
import ohos.agp.colors.HsvColor;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Image;
import ohos.agp.components.StackLayout;
import ohos.agp.components.element.VectorElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.LinearShader;
import ohos.agp.render.Paint;
import ohos.agp.render.Shader;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.agp.utils.Rect;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.multimodalinput.event.TouchEvent;


public class ColorPicker extends StackLayout
        implements Component.TouchEventListener, Component.DrawTask {
    private static final String TAG = "ColorPicker";
    private static HiLogLabel LOG_LABEL = new HiLogLabel(HiLog.LOG_APP, 0X00101, TAG);

    private float location = 1.0f;
    private float weight = 0.27f;
    private float draggingFactor = 0;
    private boolean dragging = false;

    private ColorPickerListener colorPickerListener;
    private boolean interacting;
    private boolean changingWeight;
    private boolean wasChangingWeight;

    private Image settingsButton;

    private Paint gradientPaint = new Paint();
    private Paint backgroundPaint = new Paint();
    private Paint swatchPaint = new Paint();
    private Paint swatchStrokePaint = new Paint();
    private Rect rectF = new Rect();
    private Preferences mPreferences;

    private static final int[] COLORS = new int[] {
        0xffea2739,
        0xffdb3ad2,
        0xff3051e3,
        0xff49c5ed,
        0xff80c864,
        0xfffcde65,
        0xfffc964d,
        0xff000000,
        0xffffffff
    };

    private static final Color[] COLORS_NEW = new Color[] {
        new Color(0xffea2739),
        new Color(0xffdb3ad2),
        new Color(0xff3051e3),
        new Color(0xff49c5ed),
        new Color(0xff80c864),
        new Color(0xfffcde65),
        new Color(0xfffc964d),
        new Color(0xff000000),
        new Color(0xffffffff)
    };

    private static final float[] LOCATIONS = new float[] {
        0.0f,
        0.14f,
        0.24f,
        0.39f,
        0.49f,
        0.62f,
        0.73f,
        0.85f,
        1.0f
    };

    public interface ColorPickerListener {
        void onBeganColorPicking();
        void onColorValueChanged(int color);
        void onFinishedColorPicking(int color);
        void onSettingsPressed();
    }

    public ColorPicker(Context context) {
        super(context);
        init(context);
    }

    public ColorPicker(Context context, AttrSet attributeSet) {
        super(context, attributeSet);
        init(context);
    }

    public ColorPicker(Context context, AttrSet attributeSet, String defStyle) {
        super(context, attributeSet, defStyle);
        init(context);
    }

    void init(Context context) {
        setTouchEventListener(this);
        addDrawTask(this);

        DatabaseHelper databaseHelper = new DatabaseHelper(getContext());
        mPreferences = databaseHelper.getPreferences("paint");

        gradientPaint.setAntiAlias(true);
        backgroundPaint.setAntiAlias(true);
        swatchPaint.setAntiAlias(true);
        swatchStrokePaint.setAntiAlias(true);

        Utils.checkDisplaySize(context);
        backgroundPaint.setColor(new Color(0xffffffff));
        swatchStrokePaint.setStyle(Paint.Style.STROKE_STYLE);
        swatchStrokePaint.setStrokeWidth(dp(1));
        if (settingsButton == null) {
            settingsButton = new Image(context);
        } else {
            removeComponent(settingsButton);
        }
        settingsButton.setScaleMode(Image.ScaleMode.CENTER);
        settingsButton.setBackground(new VectorElement(getContext(), ResourceTable.Graphic_ic_brush));
        addComponent(settingsButton, new ComponentContainer.LayoutConfig(dp(40), dp(32)));
        settingsButton.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                if (colorPickerListener != null) {
                    colorPickerListener.onSettingsPressed();
                }
            }
        });
        settingsButton.setTouchEventListener(new TouchEventListener() {
            @Override
        public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
                return true;
            }
            });
        location = mPreferences.getFloat("last_color_location", 0.5f);
        weight = mPreferences.getFloat("stroke", 0.27f);
        if (weight != 0.27) {
            weight /= 50.f;
        }
        setLocation(location);
    }

    private int dp(float size) {
        return (int) (size < 0 ? size : Utils.dp(size));
    }

    public void hideSettings() {
        settingsButton.setVisibility(Component.HIDE);
    }

    public void showSettings() {
        settingsButton.setVisibility(Component.VISIBLE);
    }

    public void setColorPickerListener(ColorPickerListener colorPickerListener) {
        this.colorPickerListener = colorPickerListener;
    }

    public Component getSettingsButton() {
        return settingsButton;
    }

    public void hideSettingButton() {
        settingsButton.setVisibility(Component.HIDE);
    }

    public void setSettingsButtonImage(int resId) {
        settingsButton.setBackground(new VectorElement(getContext(), resId));
    }

    public int colorForLocation(float location) {
        if (location <= 0) {
            return COLORS[0];
        } else if (location >= 1) {
            return COLORS[COLORS.length - 1];
        }

        int leftIndex = -1;
        int rightIndex = -1;

        for (int i = 1; i < LOCATIONS.length; i++) {
            float value = LOCATIONS[i];
            if (value > location) {
                leftIndex = i - 1;
                rightIndex = i;
                break;
            }
        }

        float leftLocation = LOCATIONS[leftIndex];
        int leftColor = COLORS[leftIndex];

        float rightLocation = LOCATIONS[rightIndex];
        int rightColor = COLORS[rightIndex];

        float factor = (location - leftLocation) / (rightLocation - leftLocation);
        return interpolateColors(leftColor, rightColor, factor);
    }

    private int interpolateColors(int leftColor, int rightColor, float factor) {
        factor = Math.min(Math.max(factor, 0.0f), 1.0f);

        int r1 = Utils.red(leftColor);
        int r2 = Utils.red(rightColor);

        int g1 = Utils.green(leftColor);
        int g2 = Utils.green(rightColor);

        int b1 = Utils.blue(leftColor);
        int b2 = Utils.blue(rightColor);

        int r = Math.min(255, (int) (r1 + (r2 - r1) * factor));
        int g = Math.min(255, (int) (g1 + (g2 - g1) * factor));
        int b = Math.min(255, (int) (b1 + (b2 - b1) * factor));

        return Color.argb(255, r, g, b);
    }

    public void setLocation(float value) {
        int color = colorForLocation(location = value);
        swatchPaint.setColor(new Color(color));

        float[] hsv = new float[3];
        ohos.agp.colors.RgbColor agpColor = new ohos.agp.colors.RgbColor(color);
        HsvColor hsvColor = ColorConverter.toHsv(agpColor);
        hsv[0] = hsvColor.getHue();
        hsv[1] = hsvColor.getSaturation();
        hsv[2] = hsvColor.getValue();

        if (hsv[0] < 0.001 && hsv[1] < 0.001 && hsv[2] > 0.92f) {
            int c = (int) ((1.0f - (hsv[2] - 0.92f) / 0.08f * 0.22f) * 255);
            swatchStrokePaint.setColor(new Color(Color.rgb(c, c, c)));
        } else {
            swatchStrokePaint.setColor(new Color(color));
        }

        invalidate();
    }

    public void setWeight(float value) {
        weight = value;
        invalidate();
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent event) {
        if (event.getPointerCount() > 1) {
            return false;
        }

        int index = event.getIndex();
        float x = event.getPointerScreenPosition(index).getX() - rectF.left;
        float y = event.getPointerScreenPosition(index).getY() - rectF.top - 490;

        if (!interacting && x < -Utils.dp(10)) {
            return false;
        }

        int action = event.getAction();
        if (action == TouchEvent.CANCEL || action == TouchEvent.OTHER_POINT_UP
            || action == TouchEvent.PRIMARY_POINT_UP) {
            if (interacting && colorPickerListener != null) {
                colorPickerListener.onFinishedColorPicking(colorForLocation(location));
                mPreferences.putFloat("last_color_location", location).flushSync();
            }
            interacting = false;
            wasChangingWeight = changingWeight;
            changingWeight = false;
            setDragging(false, true);
        } else if (action == TouchEvent.PRIMARY_POINT_DOWN || action == TouchEvent.POINT_MOVE) {
            if (!interacting) {
                interacting = true;
                if (colorPickerListener != null) {
                    colorPickerListener.onBeganColorPicking();
                }
            }

            float colorLocation = Math.max(0.0f, Math.min(1.0f, y / rectF.getHeight()));
            setLocation(colorLocation);
            setDragging(true, true);

            if (x < -dp(10)) {
                changingWeight = true;
                float weightLocation = (-x - dp(10)) / dp(190);
                weightLocation = Math.max(0.0f, Math.min(1.0f, weightLocation));
                setWeight(weightLocation);
            }

            if (colorPickerListener != null) {
                colorPickerListener.onColorValueChanged(colorForLocation(location));
            }
            return true;
        }
        return false;
    }

    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        int width = right - left;
        int height = bottom - top;

        int backHeight = getHeight() - dp(16) - dp(48);

        Point point1 = new Point(0, dp(16));
        Point point2 = new Point(0, backHeight + dp(16));
        Point[] points = {point1, point2};
        Shader shader = new LinearShader(points, LOCATIONS,
            COLORS_NEW, Shader.TileMode.REPEAT_TILEMODE);
        gradientPaint.setShader(shader, Paint.ShaderType.LINEAR_SHADER);
        int x = width - dp(16) - dp(8);
        int y = dp(16);
        rectF.set(x, y, x + dp(8), y + backHeight);

        settingsButton.setComponentPosition(width - settingsButton.getWidth(),
            height - dp(32), width, height);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        onLayout(true, getLeft(), getTop(), getRight(), getBottom());

        RectFloat rectFF = new RectFloat();
        rectFF.left = rectF.left;
        rectFF.right = rectF.right;
        rectFF.top = rectF.top;
        rectFF.bottom = rectF.bottom;
        canvas.drawRoundRect(rectFF, dp(6), dp(6), gradientPaint);

        int cx = (int) (rectF.getCenterX() + draggingFactor * -dp(70));
        int cy = (int) (rectF.top - dp(22) + rectF.getHeight() * location) + dp(22);

        float swatchRadius = (int) Math.floor(dp(4) + (dp(19) - dp(4))) / 1.5f;

        canvas.drawRect(cx, cy - dp(1), rectF.getCenterX(), cy + dp(1), swatchStrokePaint);
        canvas.drawCircle(cx, cy, swatchRadius, swatchPaint);
        canvas.drawCircle(cx, cy, swatchRadius - dp(0.5f), swatchStrokePaint);
    }

    private void setDraggingFactor(float factor) {
        draggingFactor = factor;
        invalidate();
    }

    public float getDraggingFactor() {
        return draggingFactor;
    }

    private void setDragging(boolean value, boolean animated) {
        if (dragging == value) {
            return;
        }
        dragging = value;
        float target = dragging ? 1.0f : 0.0f;
        if (animated) {
            AnimatorProperty a = new AnimatorProperty(this);
            a.setCurveType(Animator.CurveType.OVERSHOOT);
            int duration = 300;
            if (wasChangingWeight) {
                duration += weight * 75;
            }
            a.setDuration(duration);
            a.start();
        } else {
            setDraggingFactor(target);
        }
    }
}